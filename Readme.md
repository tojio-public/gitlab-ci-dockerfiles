hosted on Dockerhub, see https://hub.docker.com/r/tojiogmbh/drupal-8.8.5-apache

for use in gitlab-ci builds, add the following to `.gitlab-ci.yml`:

`image: tojiogmbh/drupal-8.8.5-apache:latest`
